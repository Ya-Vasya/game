﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Game001
{
    public class MyGameTime
    {

        private static readonly Lazy<MyGameTime> instance = new Lazy<MyGameTime>(() => new MyGameTime());
        public string Date { get; private set; }

        private MyGameTime()
        {
            Date = DateTime.Now.TimeOfDay.ToString();
        }

        public static MyGameTime GetInstance()
        {
            return instance.Value;
        }

    }
}
