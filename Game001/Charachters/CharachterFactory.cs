﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class CharachterFactory
    {
        Random rand = new Random();
        private Dictionary<string, CBasicCharachter> charachters = new Dictionary<string, CBasicCharachter>();
        private Texture2D classMate, teacher;

        public CharachterFactory(Texture2D classmate /*Texture2D teacher*/)
        {
            classMate = classmate;
            //this.teacher = teacher;
            charachters.Add("Bob", new ClassMate(classMate, "Carl", rand));
        }

        public CBasicCharachter createClassMate(string name)
        {
            charachters.Add(name, new ClassMate(classMate, name, rand));
            return charachters[name];
        }

        /*
        public CBasicCharachter createTeacher(string name, Subject hisClass)
        {
            charachters.Add(name, new Teacher(teachers, name, hisClass, rand));
            return charachters[name];
        }
        */

        public CBasicCharachter GetCharachter(string key)
        {
            if (charachters.ContainsKey(key))
                return charachters[key];
            else
                return null;
        }

        public List<CBasicCharachter> getCharList()
        {
            return charachters.Values.ToList();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (var ch in charachters.Keys)
            {
                if(charachters[ch] is ClassMate)
                {
                    spriteBatch.Draw(classMate, charachters[ch].Position, Color.White);
                }
            }
        }

    }
}
