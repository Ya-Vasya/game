﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    abstract class AbstractFactory
    {

        protected string _text;
        protected Texture2D _texture;
        protected Vector2 _position;
        protected string belongsTo;

        public AbstractFactory(string Text, Texture2D texture, Vector2 screenPos, string belongs)
        {
            _text = Text;
            _texture = texture;
            _position = screenPos;
            belongsTo = belongs;
        }

        public abstract BaseNotebook createNotebook(string Text, Texture2D texture, Vector2 screenPos, string belongs);
    }
}
