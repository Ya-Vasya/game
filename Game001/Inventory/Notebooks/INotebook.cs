﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    public abstract class BaseNotebook : IItem
    {

        private string innerText;
        public string InnerText { get { return innerText; } set { innerText = value; } }
        private string belongsTo;
        public string BelongsTo { get { return belongsTo; } set { belongsTo = value; } }
        private Texture2D _texture;
        public Texture2D Texture { get { return _texture; } set { _texture = value; } }
        public Vector2 screenPosition;

        public BaseNotebook(string Text, Texture2D texture, Vector2 screenPos, string belongs)
        {
            InnerText = Text;
            Texture = texture;
            screenPosition = screenPos;
            BelongsTo = belongs;
        }

        public abstract BaseNotebook Clone();

        public string readText()
        {
            return InnerText;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, screenPosition, null, Color.White);
        }

        public void Update()
        {

        }

        public void setRandomPos(Random rand)
        {
            screenPosition = new Vector2(rand.Next(0, 1920), rand.Next(0, 1080));
        }

    } 
}
