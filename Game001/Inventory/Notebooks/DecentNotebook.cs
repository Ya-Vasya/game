﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game001
{
    class DecentNotebook : BaseNotebook
    {

        public DecentNotebook(string Text, Texture2D texture, Vector2 screenPos, string belongs) : base(Text, texture, screenPos, belongs)
        {
            ///to do read data from file
        }

        public override BaseNotebook Clone()
        {
            return new DecentNotebook(InnerText, Texture, screenPosition, BelongsTo);
        }
    }
}
