﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Game001
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        CCharachter mainPlayer;
        CollisionManager collisionManager;
        Texture2D npcTexture;
        Random random;

        BaseNotebook baseNotebook;
        Texture2D notebookTexture;
        Vector2 notebookPos;
        BaseNotebook someNotebook;

        CharachterFactory charachterFactory;

        string charName = "Mark";

        Color color = Color.Black;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            Content.RootDirectory = "Content";

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;
            random = new Random();
            notebookPos = new Vector2(random.Next(0, 1920), random.Next(0, 1080));
            collisionManager = new CollisionManager();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            mainPlayer = CCharachter.getInstance(Content.Load<Texture2D>("PlayerModel"));
            npcTexture = Content.Load<Texture2D>("npc");
            notebookTexture = Content.Load<Texture2D>("Book");
            charachterFactory = new CharachterFactory(npcTexture);
            baseNotebook = new EmptyNotebook("sfe", notebookTexture, notebookPos, charName);
            charachterFactory.createClassMate("Ross");
            someNotebook = baseNotebook.Clone();
            spriteBatch = new SpriteBatch(GraphicsDevice);
            collisionManager.Main = mainPlayer;
            collisionManager.Charachters = charachterFactory.getCharList();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            collisionManager.Colide();
            mainPlayer.Update();
            baseNotebook.Update();
            someNotebook.Update();
            for (int i = 0; i < charachterFactory.getCharList().Count; i++)
            {
                charachterFactory.getCharList()[i].Update();
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            mainPlayer.Draw(spriteBatch);
            baseNotebook.Draw(spriteBatch);
            someNotebook.Draw(spriteBatch);
            charachterFactory.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
